<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Entradas;
use yii\data\ActiveDataPRovider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionListar() // Listar registros completos, devueltos como arrays no numericos
    {
        $listado = Entradas::find()->asArray()->all();

        return $this->render("listarTodos", [
            'datos' => $listado,
        ]);

    }

    public function actionListar1() // Listar registros completos, devueltos como objetos modelo
    {
        $listado = Entradas::find()->all();

        return $this->render("listarTodos", [
            'datos' => $listado,
        ]);

    }

    public function actionListar2() // Listar campos seleccionados, devueltos como array no numerico
    {
        $listado = Entradas::find()->select(['texto'])->asArray()->all();

        return $this->render("listarTodos", [
            'datos' => $listado,
        ]);

    }

    public function actionListar3() // Listar campos seleccionados, devueltos como objetos modelo
    {
        $listado = Entradas::find()->select(['texto'])->all();

        return $this->render("listarTodos", [
            'datos' => $listado,
        ]);

    }

    public function actionListar4() // Listar todo, mediante el metodo find en vez de método de clase
    {
	$e = new Entradas();

        return $this->render("listarTodos", [
            'datos' => $e->find()->all(),
        ]);

    }

    public function actionListar5() // Listar uno (con id = 1), mediante el metodo find en vez de método de clase
    {
	$e = new Entradas();

        return $this->render("listarTodos", [
            'datos' => $e->findOne(1),
        ]);

    }

    public function actionListar6() // Mostrar un listado, ejecutando una query completa tal cual
    {
	$query = Yii::$app->db->createCommand("SELECT * FROM `entradas`");

        return $this->render("listarTodos", [
            'datos' => $query->queryAll(),
        ]);

    }

    public function actionMostrar()
    {
        $dataProviderEntradas = new ActiveDataProvider([
            'query' => Entradas::find(),
        ]);

        return $this->render('mostrar',  [
            'dataProvider' => $dataProviderEntradas,
        ]);
    }

    public function actionMostraruno($id = null)
    {
        if ( empty($id) )  {
            $linklist = [];
            foreach(Entradas::find()->select(['id'])->asArray()->all() as $element)
                $linklist[] = [
                    'texto' => $element['id'],
                    'url' => "?id=".$element['id'],
                ];

	    return $this->render("listaenlaces", [
                'enlaces' => $linklist,
            ]);
        };


        $modelo = Entradas::findOne($id);

	if ($modelo == null) {
            return $this->render("listarTodos", [
                'datos' => ['No encontrado / no existe (tramposillo ^_-).'],
            ]);
	};


        return $this->render('mostraruno', [
            'model' => $modelo,
        ]);
    }


}
