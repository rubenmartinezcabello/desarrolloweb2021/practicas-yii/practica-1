<?php
    use yii\grid\GridView;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'summary' => "Mostrando los registros <strong>{begin}</strong> .. <strong>{end}</strong> de <strong>{totalCount}</strong> totales.",
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'texto',
    ],
]); ?>
