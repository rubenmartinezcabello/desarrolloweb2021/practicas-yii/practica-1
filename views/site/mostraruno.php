<?php
use yii\Widgets\DetailView;
?>

<?= DetailView::widget([
	'model' => $model,
	'attributes' => [
		'id',
		'texto',
	],
]); ?>
