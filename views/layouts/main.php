<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => /*navbar-inverse*/ 'navbar-default navbar-fixed-top',
            'id' => 'custom-bootstrap-menu',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Listados', 'items' => [
                ['label' => 'Completos como Arrays',  'url' => ['/site/listar']],
                ['label' => 'Completos como Modelos', 'url' => ['/site/listar1']],
                ['label' => 'Parciales como Arrays',  'url' => ['/site/listar2']],
                ['label' => 'Parciales como Modelos', 'url' => ['/site/listar3']],

                ['label' => 'Método no estático',     'url' => ['/site/listar4']],
                ['label' => 'Listar ID 1',            'url' => ['/site/listar5']],
                ['label' => 'Query SQL',              'url' => ['/site/listar6']],
            ]],
            ['label' => 'Mostrar', 'url' => ['/site/mostrar']],
            ['label' => 'Detalles', 'url' => ['/site/mostraruno']],
            ['label' => 'Gestión', 'url' => ['/entradas/index']],

        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Rubén Martínez</p>
        <p class="pull-right">&copy; Alpe Formación <?= date('Y') ?> <?= Yii::powered() ?></p>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
