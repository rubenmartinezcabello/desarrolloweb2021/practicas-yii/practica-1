﻿SET NAMES 'utf8mb4';

DROP DATABASE IF EXISTS `h1`;

CREATE DATABASE `h1`
  CHARACTER SET UTF8MB4
  COLLATE utf8mb4_general_ci;



CREATE TABLE `h1`.`entradas` (
  `id`    int(11)      NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;

INSERT INTO h1.entradas(id, texto) VALUES
(1, 'Ejemplo de prueba'),
(2, 'Otro más.'),
(3, 'Y otro pa''l bote.');